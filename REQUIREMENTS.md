# Requirements
## Wp Plugins
 - WP REST API Menus
 
 
 ## GatsbyConfig

 ```
 includedRoutes: [
  "**/posts",
  "**/pages",
  "**/media",
  "**/categories",
  "**/tags",
  "**/taxonomies",
  "**/users",
  "**/wp-api-menus/v2/menus",
],
```